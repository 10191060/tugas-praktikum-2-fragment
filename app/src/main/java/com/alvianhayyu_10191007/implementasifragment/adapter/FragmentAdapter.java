package com.alvianhayyu_10191007.implementasifragment.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.alvianhayyu_10191007.implementasifragment.fragment.HomeFragment;
import com.alvianhayyu_10191007.implementasifragment.fragment.StatusFragment;

public class FragmentAdapter extends FragmentPagerAdapter {
    private Context context;
    private int tabsCount;

    public FragmentAdapter(Context context, int tabsCount, FragmentManager fragmentManager) {
        super(fragmentManager);

        this.context = context;
        this.tabsCount = tabsCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                StatusFragment statusFragment = new StatusFragment();
                return statusFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabsCount;
    }
}
